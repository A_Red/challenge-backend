import aiohttp
import asyncio
import json
import logging
import motor.motor_asyncio

from aiohttp import web
from config import config
from constants import LANGUAGE, SEARCHING_FIELDS, HTTP_STATUS_CODES, \
    URL_GITHUB_REPOSITORIES, HTTP_METHODS, HTTP_STATUS_CODES_TO_RETRY, VERSION
from os import path


dbapi_string = config['db']
port = config['serving_at']
client = motor.motor_asyncio.AsyncIOMotorClient(dbapi_string)
db = client.github

logging.basicConfig(filename=path.join(config['log_path'], "server.log"),
                    level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')

async def handler(request):
    """
    HTTP Handler that handle GET-request with 2 parameters:
    - language
    - stars
    Working with GitHub Search API. Looking for repositories with filter's
    parameters language and stars(more than).

    Processing response data and store it.

    :param request: (obj)
    :return: (obj) JSON
    """
    logging.info('GitHub repositories response')

    language = request.rel_url.query.get('language')
    stars = int(request.rel_url.query.get('stars', 0))

    page = int(request.rel_url.query.get('page', 1))#default page is 1
    per_page = int(request.rel_url.query.get('per_page', 30))#paginated to 30 items by default

    if per_page > 100:
        per_page = 100 #up to a maximum of 100 items

    input_dict = {
        'language': language,
        'stars': str(stars),
    }

    # Authentication user could send 5000 request per hour
    # Unauthentication user could send 60 request per hour
    # auth = aiohttp.BasicAuth(login=config['username'],
    #                          password=config['password'])
    auth = None

    # Send GET-request to API GitHub Search
    if language == LANGUAGE['python'] and stars > 500:
        async with aiohttp.ClientSession(auth=auth) as session:
            query_without_stars = '+'.join(
                [':'.join([k, v]) for k, v in input_dict.items() if k != 'stars'])
            query = ' '.join([query_without_stars, 'stars:>500'])

            params = {
                'q': query,
                'sort': 'stars', # Bonus 1 - Sorting by stars
                'page': page,
                'per_page': per_page
            }

            async with session.get(URL_GITHUB_REPOSITORIES,
                                   params=params) as response:
                pagination = response.headers.get('Link')
                if response.status == HTTP_STATUS_CODES['200']:
                    try:
                        data = await response.json()
                    except json.JSONDecodeError as e:
                        logging.error('failed to decode response code:%s url:%s'
                                      ' method:%s error:%s response:%s',
                                      response.status, URL_GITHUB_REPOSITORIES,
                                      HTTP_METHODS['GET'], e, response.reason)
                        raise aiohttp.web.HTTPServerError(
                            status=response.status, text=e.msg)
                elif response.status in HTTP_STATUS_CODES_TO_RETRY:
                    logging.error('received invalid response code:%s url:%s '
                                  'method: %s error:%s response:%s',
                                  response.status, URL_GITHUB_REPOSITORIES,
                                  HTTP_METHODS['GET'], '', response.reason)
                    raise aiohttp.web.HTTPServerError(status=response.status,
                                                      text=response.reason)
                else:
                    try:
                        data = await response.json()
                    except json.JSONDecodeError as e:
                        logging.error('failed to decode response code:%s url:%s'
                                      ' method:%s error:%s response:%s',
                                      response.status, URL_GITHUB_REPOSITORIES,
                                      HTTP_METHODS['GET'], e, response.reason)
                        raise aiohttp.web.HTTPClientError(text=e.msg,
                                                          status=response.status)
                    else:
                        logging.warning('received %s for %s' % data,
                                        URL_GITHUB_REPOSITORIES)

        # Couldn't find at API GitHub Search filters for choosing current fields from JSON
        # Thats why Get fields 'full_name', html_url','description','stargazers_count' and
        # 'language' from response JSON
        result_data = []
        for x in data['items']:
            temporary_dict = {}
            for y in SEARCHING_FIELDS:
                temporary_dict[y] = x.get(y)
            result_data.append(temporary_dict)

        store_data = await db.repositories.insert_one(result_data)

    else:
        result_data = [
            {'description': 'Choose python language and more 500 stars'}]
        pagination = None

    document = {'items': result_data,
                'pagination': pagination
    }

    response = web.Response(content_type='application/json')
    response.text = json.dumps(document)
    return response

async def init(loop):
    handler = app.make_handler()
    srv = await loop.create_server(handler, '0.0.0.0', port)
    sock = srv.sockets[0].getsockname()
    print('Serving on', sock)
    logging.info('Server started on ' + str(sock))
    return srv

loop = asyncio.get_event_loop()
app = web.Application()

# Put all possible handlers definition here like this
app.router.add_get('/github/searh/%s' % (VERSION,), handler)

loop.run_until_complete(init(loop))

if __name__ == '__main__':
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    logging.info('Server stopped.\n')