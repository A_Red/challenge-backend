LANGUAGE = {'python': 'python'}

SEARCHING_FIELDS = [
    'full_name',
    'html_url',
    'description',
    'stargazers_count',
    'language'
]

HTTP_STATUS_CODES = {
    '200': 200,
    '400': 400,
    '422': 422,
    '500': 500
}

HTTP_STATUS_CODES_TO_RETRY = [500, 502, 503, 504]

HTTP_METHODS = {
    'GET': 'GET',
    'POST': 'POST'
}

URL_GITHUB_REPOSITORIES = 'https://api.github.com/search/repositories'

VERSION = 'v0.1'